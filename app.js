import express from 'express';
import cors from 'cors';
import helmet from 'helmet';
import bodyParser from 'body-parser';
import rateLimit from 'express-rate-limit';
import winston from 'winston';
import expressWinston from 'express-winston';
import csrf from  'csurf';
import session from 'express-session';
import cookieParser from 'cookie-parser';
import errHndlr from './src/utils/errors';
import 'winston-daily-rotate-file';
import 'dotenv/config'

// mongo connection
import './config/mongoDb';

// mysql connection
import './config/mySql';

// require routes
import sample from './src/routes/sampleRoute';

// Initializing express app
const app = express();

// Adds helmet middleware
app.use(helmet());

//Etag disable
app.set('etag', false);

//Body Parser Configuration
app.use(bodyParser.json({ // to support JSON-encoded bodies
  limit: '1mb'
}));

app.use(bodyParser.urlencoded({ // to support URL-encoded bodies
  limit: '1mb',
  extended: true
}));

// Using CORS
app.use(cors());

//Rate Limit for API
app.enable('trust proxy');  // only if you're behind a reverse proxy (Heroku, Bluemix, AWS if you use an ELB, custom Nginx setup, etc

const limiter = new rateLimit({
  windowMs: 60 * 1000, // 1 minutes
  max: 50, // limit each IP to 100 requests per windowMs
  delayMs: 0 // disable delaying - full speed until the max limit is reached
});

//  apply to all requests
app.use(limiter);

// winston Configuration
expressWinston.requestWhitelist.push('body');
expressWinston.responseWhitelist.push('body');
expressWinston.bodyBlacklist.push('backupkey', 'password', 'pin', 'mPass', 'keyObject');
app.use(expressWinston.logger({
  transports: [
    new (winston.transports.DailyRotateFile)({
      dirname: './logs',
      filename: 'access-%DATE%.log',
      datePattern: 'YYYY-MM-DD-HH',
      zippedArchive: true,
      maxSize: '20m',
      maxFiles: '1d'
    })]
}));

// express-seeeions config
app.use(session({
  secret: 'My super session secret',
  cookie: {
    httpOnly: true,
    secure: true,
  },
  resave: false,
  saveUninitialized: false,
}));

app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());

// csurf config
// app.use(csrf({ cookie: true }));

// Router Initialization
app.get('/v1', (req, res) => {
  res.status(200).json({
    success: true,
    msg: 'Welcome to Boilerplate v1.0',
    data: {}
  });
});
app.use('/v1/sample', sample);


module.exports = app;

