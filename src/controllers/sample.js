const User = require('../models/sample2');
import { ValidationError, PermissionError, AuthorizationError,
         DatabaseError, NotFoundError, OperationalError
       } from '../utils/errors';
import sample from '../models/sample';
import jwt from '../middlewares/jwt';

class SampleData {
  constructor() {}

  async getSampleData(req, res) {
    let data = await sample.getData();
    return res.status(200).json({
      success: true,
      msg: 'Sample Data.',
      data: { serviceProvider : data }
    });
  }

  async createToken(req, res){
    let date = Math.floor(Date.now() / 1000); // will generate token with custom time in seconds.
    let token = await jwt.generateToken({ sample : 'LalaWorld', iat : date }, '1200s', req.originalUrl);
    res.status(200).json({success:true,msg:'Token created',data:{token:token}});
  }

  async sendTokenData(req, res){
    return res.status(200).json({
        success: true,
        msg: 'Decoded Data.',
        data: req._decoded 
      });
  }
}

module.exports = new SampleData();