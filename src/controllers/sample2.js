const User = require('../models/sample2');
import {
  ValidationError,
  PermissionError,
  AuthorizationError,
  DatabaseError,
  NotFoundError,
  OperationalError
} from '../utils/errors';

class UserData {
  constructor() {}

  async getData(req, res) {
    return res.status(200).json({
      success: true,
      msg: 'sample2',
      data: {}
    });
  }

  async login(req, res, next) {
    const username = req.parsed.username;
    let foundUser = null;
    try {
      foundUser = await User.findOne({
        username
      });
    } catch (error) {
      return next(new AuthorizationError('Username or password do not match'));
    }

    if (foundUser === null || foundUser.password !== req.parsed.password) {
      return next(new NotFoundError('Username or password do not match'));
    }
    next();
  };

  async getUser(req, res) {
    const username = req.params.username;
    try {
      const user = await User.findOne({
        username
      });
      if (user === null) {
        return next(new NotFoundError('No user in the database'));
      }

      res.status(200).json({
        success: true,
        msg: 'User found successfully',
        data: {
          username,
          location: user.location
        }
      });
    } catch (err) {
      return next(new DatabaseError('Error in fetching user Info'));
    }
  };

  async register(req, res) {
    const user = new User();
    user.username = req.parsed.username;
    user.password = req.parsed.password;
    user.location = req.parsed.location;
    try {
      await user.save();
      res.status(201).json({
        success: true,
        msg: 'User successfuly registered',
        data: {}
      });
    } catch (error) {
      next(new DatabaseError('Error in fetching user Info'));
    }
  };

  async update(req, res) {
    const username = req.parsed.username;
    let user = null;
    try {
      user = await User.findOne({
        username
      });
    } catch (error) {
      res.status(400).json({
        success: false,
        msg: 'Some error occurred.',
        data: {}
      });
    }
    if (user === null) {
      return next(new NotFoundError('No user in the database'));
    }

    user.location = req.body.location;
    try {
      await user.save();
      res.status(200).json({
        success: true,
        msg: 'User successfuly updated',
        data: {
          location: user.location
        }
      });
    } catch (error) {
      next(new DatabaseError('Could not update User'));
    }
  };
}

module.exports = new UserData();