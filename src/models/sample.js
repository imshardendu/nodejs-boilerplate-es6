'use strict';
import conn from '../../config/mySql';

class GetSampleData {
  constructor () {}
  async getData () {
    return new Promise((resolve, reject)=>{
      conn.query (
        `select code, name, description from service_providers `,
        (err, result) => {
          if (err) return reject (err);
          else {
            const string = JSON.stringify (result);
            const json = JSON.parse (string);
            return  resolve(json);
          }
        });
    })
  }
}

module.exports = new GetSampleData();