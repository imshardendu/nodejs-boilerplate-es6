import express from 'express';
import sample  from '../controllers/sample';
import validate from '../controllers/validator';
import { errorHandler } from '../middlewares/errors';
import jwt from '../middlewares/jwt';

const router = express.Router();

router.get('/sendToken', sample.createToken);
router.get('/verifyToken', jwt.verifyToken ,sample.sendTokenData);
router.get('/demo',jwt.verifyToken ,sample.getSampleData);

router.use(errorHandler);

module.exports = router;